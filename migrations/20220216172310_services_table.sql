-- +goose Up
-- +goose StatementBegin
create table if not exists services
(
    id         uuid not null
        constraint services_pkey
            primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title      text,
    "group"    text
);

create index if not exists idx_services_deleted_at
    on services (deleted_at);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table services;
-- +goose StatementEnd
