-- +goose Up
-- +goose StatementBegin
create table if not exists deploys
(
    id                 uuid not null
        constraint deploys_pkey
            primary key,
    created_at         timestamp with time zone,
    updated_at         timestamp with time zone,
    deleted_at         timestamp with time zone,
    branch             text,
    status             text,
    deployment_task_id uuid
        constraint fk_deployment_tasks_deploys
            references deployment_tasks,
    service_id         uuid
        constraint fk_deploys_service
            references services,
    hash               text
);

create index if not exists idx_deploys_deleted_at
    on deploys (deleted_at);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table deploys;
-- +goose StatementEnd
