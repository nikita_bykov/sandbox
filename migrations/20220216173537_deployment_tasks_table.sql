-- +goose Up
-- +goose StatementBegin
create table if not exists deployment_tasks
(
    id         uuid not null
        constraint deployment_tasks_pkey
            primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title      text,
    status     text
);

create index if not exists idx_deployment_tasks_deleted_at
    on deployment_tasks (deleted_at);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table deployment_tasks;
-- +goose StatementEnd
