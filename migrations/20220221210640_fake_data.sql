-- +goose Up
-- +goose StatementBegin
INSERT INTO services (id, created_at, updated_at, deleted_at, title, "group") VALUES ('a1bb8f2a-9991-4c5c-9832-3101d68628a1', '2022-02-21 20:58:18.759000 +00:00', '2022-02-21 20:58:20.433000 +00:00', null, 'vendor-office', 'vof') ON CONFLICT ON CONSTRAINT services_pkey DO NOTHING;
INSERT INTO services (id, created_at, updated_at, deleted_at, title, "group") VALUES ('34e6bc66-e9b7-4374-a462-314b8d8540bf', '2022-02-21 20:58:39.359000 +00:00', '2022-02-21 20:58:41.762000 +00:00', null, 'vendor-onboarding', 'vob') ON CONFLICT ON CONSTRAINT services_pkey DO NOTHING;

INSERT INTO namespaces (id, created_at, updated_at, deleted_at, title) VALUES ('736a22a3-9284-435f-a3ae-1f31a6ccfd7d', '2022-02-21 20:57:47.234000 +00:00', '2022-02-21 20:57:49.389000 +00:00', null, 'Feature-namespace-1') ON CONFLICT ON CONSTRAINT namespaces_pkey DO NOTHING;

INSERT INTO namespace_services (namespace_id, service_id) VALUES ('736a22a3-9284-435f-a3ae-1f31a6ccfd7d', 'a1bb8f2a-9991-4c5c-9832-3101d68628a1');
INSERT INTO namespace_services (namespace_id, service_id) VALUES ('736a22a3-9284-435f-a3ae-1f31a6ccfd7d', '34e6bc66-e9b7-4374-a462-314b8d8540bf');
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
