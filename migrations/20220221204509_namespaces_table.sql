-- +goose Up
-- +goose StatementBegin
create table if not exists namespaces
(
    id         uuid not null
        constraint namespaces_pkey
            primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title      text
);

create index if not exists idx_namespaces_deleted_at
    on namespaces (deleted_at);

create table if not exists namespace_services
(
    namespace_id uuid not null
        constraint fk_namespace_services_namespace
            references namespaces,
    service_id   uuid not null
        constraint fk_namespace_services_service
            references services,
    constraint namespace_services_pkey
        primary key (namespace_id, service_id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table namespace_services;
drop table namespaces;
-- +goose StatementEnd
