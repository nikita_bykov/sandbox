package git

type GetLastHashService struct{}

func (s *GetLastHashService) GetHash(serviceName *string, serviceGroup *string, branch *string) (*Hash, error) {
	if serviceName != nil && serviceGroup != nil && branch != nil {
		h := Hash("d2920b0ec29")
		return &h, nil
	}
	return nil, nil
}
