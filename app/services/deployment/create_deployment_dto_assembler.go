package deployment

import (
	"github.com/google/uuid"
	"local/sandbox/app/services/deployment/dto"
)

type CreateDeploymentDtoAssembler struct {}

func (a *CreateDeploymentDtoAssembler) Assemble() *dto.CreateDeploymentDto {
	namespaceId, _ := uuid.Parse("736a22a3-9284-435f-a3ae-1f31a6ccfd7d")
	serviceIdA, _ := uuid.Parse("a1bb8f2a-9991-4c5c-9832-3101d68628a1")
	serviceIdB, _ := uuid.Parse("34e6bc66-e9b7-4374-a462-314b8d8540bf")

	createDeploymentDto := dto.NewCreateDeploymentDto(
		namespaceId,
		[]uuid.UUID{serviceIdA, serviceIdB},
	)

	return createDeploymentDto
}
