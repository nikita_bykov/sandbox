package deployment

import (
	"local/sandbox/app/entities"
	"local/sandbox/app/entities/vo"
	"local/sandbox/app/repository"
	"local/sandbox/app/services/deployment/dto"
	"local/sandbox/app/services/git"
)

type CreateService struct {
	deploymentTaskRepository repository.DeploymentTaskRepository
	namespaceRepository      repository.NamespaceRepository
	serviceRepository        repository.ServiceRepository
	hashService              *git.GetLastHashService
}

func NewCreateService(
	deploymentTaskRepository repository.DeploymentTaskRepository,
	namespaceRepository repository.NamespaceRepository,
	serviceRepository repository.ServiceRepository,
	hashService *git.GetLastHashService,
) *CreateService {
	return &CreateService{
		deploymentTaskRepository: deploymentTaskRepository,
		namespaceRepository:      namespaceRepository,
		serviceRepository:        serviceRepository,
		hashService:              hashService,
	}
}

func (cs *CreateService) Process(dto *dto.CreateDeploymentDto) *entities.DeploymentTask {

	var deploys []*entities.Deploy
	branch := "master"

	for _, serviceId := range dto.Services {

		service := cs.serviceRepository.Get(serviceId)

		h, _ := cs.hashService.GetHash(service.Title, service.Group, &branch)
		var hash = *h

		deploy, _ := entities.NewDeploy(string(hash), vo.DEPLOY_STATUS_NEW, service)
		deploys = append(deploys, deploy)
	}

	dt, _ := entities.NewDeploymentTask("", "new", deploys)
	cs.deploymentTaskRepository.Create(dt)
	return dt
}
