package dto

import "github.com/google/uuid"

type CreateDeploymentDto struct {
	NamespaceId uuid.UUID
	Services    []uuid.UUID
}

func NewCreateDeploymentDto(namespaceId uuid.UUID, services []uuid.UUID) *CreateDeploymentDto {
	return &CreateDeploymentDto{
		NamespaceId: namespaceId,
		Services:    services,
	}
}
