package main

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"local/sandbox/app/repository"
	"local/sandbox/app/services/deployment"
	"local/sandbox/app/services/git"
)

func main() {

	// Setup
	dsn := "host=db user=admin password=example dbname=admin port=5432 sslmode=disable TimeZone=Europe/Moscow"
	db, _ := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	// DI
	deploymentTaskRepository := repository.NewDeploymentTaskRepository(db)
	namespaceRepository := repository.NewNamespaceRepository(db)
	serviceRepository := repository.NewServiceRepository(db)
	gitHashService := &git.GetLastHashService{}
	createService := deployment.NewCreateService(
		deploymentTaskRepository,
		namespaceRepository,
		serviceRepository,
		gitHashService,
	)
	createDeploymentDtoAssembler := &deployment.CreateDeploymentDtoAssembler{}

	// DTO
	createDeploymentDto := createDeploymentDtoAssembler.Assemble()

	// Logic
	dtInMemory := createService.Process(createDeploymentDto)
	dtInDb := deploymentTaskRepository.Get(dtInMemory.ID)

	fmt.Println("Data from memory:")
	spew.Dump(dtInMemory)

	fmt.Println("Data from DB:")
	spew.Dump(dtInDb)
}
