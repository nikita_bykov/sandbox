package entities

import "github.com/google/uuid"

type Service struct {
	Entity
	Title *string
	Group *string
}

func NewService(title string, group string) (*Service, error) {
	id := uuid.New()
	return &Service{
		Entity: Entity{
			ID: &id,
		},
		Title: &title,
		Group: &group,
	}, nil
}
