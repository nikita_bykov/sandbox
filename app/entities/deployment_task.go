package entities

import (
	"github.com/google/uuid"
	"local/sandbox/app/entities/vo"
)

type DeploymentTask struct {
	Entity
	Title   *string
	Status  *vo.DeploymentTaskStatus
	Deploys []*Deploy
}

func NewDeploymentTask(title string, status string, deploys []*Deploy) (*DeploymentTask, error) {
	var d DeploymentTask
	s, err := vo.NewDeploymentTaskStatus(status)
	if err == nil {
		id := uuid.New()
		d = DeploymentTask{
			Entity: Entity{
				ID: &id,
			},
			Title:   &title,
			Status:  s,
			Deploys: deploys,
		}
	}
	return &d, err
}
