package entities

import "time"
import "github.com/google/uuid"

type Entity struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
	DeletedAt *time.Time `gorm:"index"`
}
