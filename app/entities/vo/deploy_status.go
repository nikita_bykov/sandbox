package vo

import (
	"errors"
	"fmt"
)

const (
	DEPLOY_STATUS_NEW               = "new"
	DEPLOY_STATUS_BUILD_IN_PROCESS  = "build_in_process"
	DEPLOY_STATUS_DEPLOY_IN_PROCESS = "deploy_in_process"
	DEPLOY_STATUS_DONE              = "done"
	DEPLOY_STATUS_FAILED            = "failed"
	DEPLOY_STATUS_CANCELED          = "canceled"
)

type DeployStatus string

func NewDeployStatus(statusName string) (*DeployStatus, error) {

	statuses := map[string]bool{
		DEPLOY_STATUS_NEW:               true,
		DEPLOY_STATUS_BUILD_IN_PROCESS:  true,
		DEPLOY_STATUS_DEPLOY_IN_PROCESS: true,
		DEPLOY_STATUS_DONE:              true,
		DEPLOY_STATUS_FAILED:            true,
		DEPLOY_STATUS_CANCELED:          true,
	}

	if _, ok := statuses[statusName]; ok == true {
		s := DeployStatus(statusName)
		return &s, nil
	}

	return nil, errors.New(fmt.Sprintf("Status '%s' is not supported", statusName))
}
