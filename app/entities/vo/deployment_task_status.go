package vo

import (
	"errors"
	"fmt"
)

const (
	DEPLOYMENT_STATUS_NEW         = "new"
	DEPLOYMENT_STATUS_IN_PROGRESS = "in_progress"
	DEPLOYMENT_STATUS_DONE        = "done"
	DEPLOYMENT_STATUS_FAILED      = "failed"
	DEPLOYMENT_STATUS_CANCELED    = "canceled"
)

type DeploymentTaskStatus string

func NewDeploymentTaskStatus(statusName string) (*DeploymentTaskStatus, error) {

	statuses := map[string]bool{
		DEPLOYMENT_STATUS_NEW:         true,
		DEPLOYMENT_STATUS_IN_PROGRESS: true,
		DEPLOYMENT_STATUS_DONE:        true,
		DEPLOYMENT_STATUS_FAILED:      true,
		DEPLOYMENT_STATUS_CANCELED:    true,
	}

	if _, ok := statuses[statusName]; ok == true {
		s := DeploymentTaskStatus(statusName)
		return &s, nil
	}

	return nil, errors.New(fmt.Sprintf("Status '%s' is not supported", statusName))
}
