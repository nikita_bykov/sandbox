package entities

import (
	"github.com/google/uuid"
	"local/sandbox/app/entities/vo"
)

type Deploy struct {
	Entity
	Hash           *string
	Status           *vo.DeployStatus
	DeploymentTaskID *uuid.UUID
	ServiceID        *uuid.UUID
	Service          *Service
}

func NewDeploy(hash string, status string, service *Service) (*Deploy, error) {
	var d Deploy
	s, err := vo.NewDeployStatus(status)
	if err == nil {
		id := uuid.New()
		d = Deploy{
			Entity: Entity{
				ID: &id,
			},
			Hash:  &hash,
			Status:  s,
			Service: service,
		}
	}

	return &d, err
}
