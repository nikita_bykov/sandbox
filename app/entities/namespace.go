package entities

import "github.com/google/uuid"

type Namespace struct {
	Entity
	Title    *string
	Services []*Service `gorm:"many2many:namespace_services;"`
}

func NewNamespace(title string, services []*Service) (*Namespace, error) {
	id := uuid.New()
	return &Namespace{
		Entity: Entity{
			ID: &id,
		},
		Title:    &title,
		Services: services,
	}, nil
}
