package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"local/sandbox/app/entities"
)

type serviceRepository struct {
	db *gorm.DB
}

func NewServiceRepository(db *gorm.DB) ServiceRepository {
	return &serviceRepository{
		db: db,
	}
}

func (nr *serviceRepository) Get(id uuid.UUID) *entities.Service {
	var service entities.Service
	nr.db.Take(&service, "id = ?", id.String())
	return &service
}
