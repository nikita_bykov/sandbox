package repository

import (
	"github.com/google/uuid"
	"local/sandbox/app/entities"
)

type DeploymentTaskRepository interface {
	Create(entity *entities.DeploymentTask)
	Get(id *uuid.UUID) *entities.DeploymentTask
}

type DeployRepository interface {
	Get(id *uuid.UUID) *entities.Deploy
}

type NamespaceRepository interface {
	Get(id uuid.UUID) *entities.Namespace
}

type ServiceRepository interface {
	Get(id uuid.UUID) *entities.Service
}