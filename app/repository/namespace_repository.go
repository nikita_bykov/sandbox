package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"local/sandbox/app/entities"
)

type namespaceRepository struct {
	db *gorm.DB
}

func NewNamespaceRepository(db *gorm.DB) NamespaceRepository {
	return &namespaceRepository{
		db: db,
	}
}

func (nr *namespaceRepository) Get(id uuid.UUID) *entities.Namespace {
	var namespace entities.Namespace
	nr.db.Preload("Services").Take(&namespace, "id = ?", id.String())
	return &namespace
}
