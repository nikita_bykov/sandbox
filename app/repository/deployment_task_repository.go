package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"local/sandbox/app/entities"
)

type deploymentTaskRepository struct {
	db *gorm.DB
}

func NewDeploymentTaskRepository(db *gorm.DB) DeploymentTaskRepository {
	return &deploymentTaskRepository{
		db: db,
	}
}

func (dtr *deploymentTaskRepository) Create(entity *entities.DeploymentTask) {
	dtr.db.Create(entity)
}

func (dtr *deploymentTaskRepository) Get(id *uuid.UUID) *entities.DeploymentTask {
	var deploymentTask entities.DeploymentTask
	dtr.db.Preload("Deploys").Take(&deploymentTask, "id = ?", id.String())
	return &deploymentTask
}
